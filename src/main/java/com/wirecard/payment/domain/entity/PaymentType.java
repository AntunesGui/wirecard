package com.wirecard.payment.domain.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public enum PaymentType {

	BOLETO("boleto"),
	CARD("cartao");

	private String type;

	private PaymentType(String type) {
		this.type = type;
	}
}