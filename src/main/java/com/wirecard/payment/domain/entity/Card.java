package com.wirecard.payment.domain.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
@NoArgsConstructor
public class Card {
	
	@JsonProperty("holder_name")
	private String holderName;
	
	@JsonProperty("number")
	private String number;
	
	@JsonProperty("expiration_date")
	private String expirationDate;
	
	@JsonProperty("verification_code")
	private Integer cvv;
}
