package com.wirecard.payment.domain.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
@NoArgsConstructor
public class Buyer {

	@JsonProperty("client")
	private Client client;

	@JsonProperty("name")
	private String name;

	@JsonProperty("email")
	private String email;

	@JsonProperty("cpf")
	private Long cpf;

	@JsonProperty("password")
	private String passWord;
}