package com.wirecard.payment.domain.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public enum StatusPayment {
	
	BOLETONUMBER("23790.50400 41991.029202 58008.109207 1 80600000019900"),
	SUCCESS("Transaction approved"),
	FAILURE("Transaction not approved");
	
	private String status;

	private StatusPayment(String status) {
		this.status = status;
	}
}
