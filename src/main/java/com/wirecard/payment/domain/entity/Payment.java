package com.wirecard.payment.domain.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.*;
import lombok.experimental.Accessors;

@Data
@Getter
@Setter
@Builder
@ToString
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"status", "amount","type", "boleto", "card" })
public class Payment {
	
	@JsonProperty("amount")
	private Double amount;
	
	@JsonProperty("type")
	private PaymentType paymentType;
	
	@JsonProperty("boleto")
	private String boletoNumber;
	
	@JsonProperty("card")
	private Card card;
	
	@JsonProperty("status")
	private StatusPayment statusPayment;
	
	@JsonProperty("buyer")
	private Buyer buyer;
}