package com.wirecard.payment.usecases;

import com.wirecard.payment.domain.entity.Payment;
import com.wirecard.payment.domain.entity.StatusPayment;
import com.wirecard.payment.domain.exception.WireCardException;
import com.wirecard.payment.usecases.port.PaymentRepositoryPort;
import com.wirecard.payment.usecases.port.ProcessPaymentPort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("byCredit")
public class ProcessPaymentByCredit implements ProcessPaymentPort {

    @Autowired
    private ValidateCreditCard validateCreditCard;

    @Autowired
    private CheckCardPaymentTransaction cardTransaction;

    @Autowired
    private PaymentRepositoryPort repository;

    @Override
    public String processPayment(Payment payment) throws WireCardException {
        payment.setStatusPayment(StatusPayment.FAILURE);
        if (cardTransaction.checkPaymentTransactionIsAllowed(payment) &&
                validateCreditCard.isCardValid(payment)) {
            payment.setStatusPayment(StatusPayment.SUCCESS);
        }
        repository.save(payment);
        return payment.getStatusPayment().getStatus();
    }
}
