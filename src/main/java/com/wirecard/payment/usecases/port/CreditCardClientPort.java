package com.wirecard.payment.usecases.port;

import com.wirecard.payment.domain.entity.Payment;

public interface CreditCardClientPort {
	
	Boolean checkPaymentAllowed(Payment payment);

}
