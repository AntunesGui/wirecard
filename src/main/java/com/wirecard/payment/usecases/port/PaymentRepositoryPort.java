package com.wirecard.payment.usecases.port;

import com.wirecard.payment.domain.entity.Payment;
import org.springframework.transaction.annotation.Transactional;

public interface PaymentRepositoryPort {

	@Transactional
	void save(Payment payment);
	@Transactional
	Payment findById(String idPayment);
}
