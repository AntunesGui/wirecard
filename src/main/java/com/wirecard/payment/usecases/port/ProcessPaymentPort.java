package com.wirecard.payment.usecases.port;

import com.wirecard.payment.domain.entity.Payment;
import com.wirecard.payment.domain.exception.WireCardException;
import org.springframework.stereotype.Service;

@Service
public interface ProcessPaymentPort {

    public String processPayment(Payment payment) throws WireCardException;
}
