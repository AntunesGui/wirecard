package com.wirecard.payment.usecases;

import com.wirecard.payment.domain.entity.Payment;
import com.wirecard.payment.domain.entity.StatusPayment;
import com.wirecard.payment.domain.exception.WireCardException;
import com.wirecard.payment.usecases.port.PaymentRepositoryPort;
import com.wirecard.payment.usecases.port.ProcessPaymentPort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("byBoleto")
public class ProcessPaymentByBoleto implements ProcessPaymentPort {

    @Autowired
    private PaymentRepositoryPort repository;

    @Override
    public String processPayment(Payment payment) throws WireCardException {
        repository.save(Payment.builder()
        .statusPayment(StatusPayment.SUCCESS)
        .boletoNumber(StatusPayment.BOLETONUMBER.getStatus())
        .build());
        return payment.getBoletoNumber();
    }
}
