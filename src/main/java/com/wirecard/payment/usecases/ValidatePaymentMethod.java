package com.wirecard.payment.usecases;

import org.springframework.stereotype.Service;

import com.wirecard.payment.domain.entity.Payment;
import com.wirecard.payment.domain.entity.PaymentType;

@Service
public class ValidatePaymentMethod  {

	public Boolean isCardPaymentMethod(Payment payment) {
		return payment.getPaymentType().equals(PaymentType.CARD);
	}

	public Boolean isBoletoPaymentMethod(Payment payment) {
		return payment.getPaymentType().equals(PaymentType.BOLETO);
	}
}
