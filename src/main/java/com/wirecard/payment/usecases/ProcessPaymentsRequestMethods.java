package com.wirecard.payment.usecases;

import com.wirecard.payment.domain.entity.Payment;
import com.wirecard.payment.domain.exception.WireCardException;
import com.wirecard.payment.usecases.port.PaymentRepositoryPort;
import com.wirecard.payment.usecases.port.ProcessPaymentPort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class ProcessPaymentsRequestMethods {

	@Autowired
	@Qualifier("byBoleto")
	private ProcessPaymentPort processByBoleto;

	@Autowired
	@Qualifier("byCredit")
	private ProcessPaymentPort processByCredit;

	@Autowired
	private ValidatePaymentMethod validate;

	@Autowired
	private PaymentRepositoryPort repository;
	
	public Payment findPayment(String id) {
		return repository.findById(id);
	}

	public String process(Payment payment) throws WireCardException {
		if (validate.isCardPaymentMethod(payment)) {
			return processByCredit.processPayment(payment);
		}
		return processByBoleto.processPayment(payment);
	}
}
