package com.wirecard.payment.usecases;

import com.wirecard.payment.domain.entity.Payment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@Service
public class ValidateCreditCard {

	private static final String CARD_VALIDATION = "Is card valid: ";
	private static final String ISSUER_CARD = "Issuer card is: ";

	public Boolean isCardValid(Payment payment) {

		String regex = "^(?:(?<visa>4[0-9]{12}(?:[0-9]{3})?)|" +
				"(?<mastercard>5[1-5][0-9]{14})|" +
				"(?<discover>6(?:011|5[0-9]{2})[0-9]{12})|" +
				"(?<amex>3[47][0-9]{13})|" +
				"(?<diners>3(?:0[0-5]|[68][0-9])?[0-9]{11})|" +
				"(?<jcb>(?:2131|1800|35[0-9]{3})[0-9]{11}))$";
		
		String cardNumber = payment.getCard().getNumber();
		String issuerName = payment.getCard().getHolderName();
		Pattern pattern = Pattern.compile(regex);
		cardNumber = cardNumber.replaceAll("-", "");
		Matcher matcher = pattern.matcher(cardNumber);
		log.info(CARD_VALIDATION.concat(String.valueOf(matcher.matches())).concat(" - ").concat(ISSUER_CARD).concat(issuerName));
		return matcher.matches();
	}
}