package com.wirecard.payment.usecases;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wirecard.payment.domain.entity.Payment;
import com.wirecard.payment.usecases.port.CreditCardClientPort;

@Service
public class CheckCardPaymentTransaction  {
	
	@Autowired
	private CreditCardClientPort cardClient;
	
	public Boolean checkPaymentTransactionIsAllowed(Payment payment) {
		return cardClient.checkPaymentAllowed(payment);
	}
}
