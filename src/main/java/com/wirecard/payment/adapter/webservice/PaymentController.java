package com.wirecard.payment.adapter.webservice;

import com.wirecard.payment.domain.entity.Payment;
import com.wirecard.payment.domain.entity.StatusPayment;
import com.wirecard.payment.usecases.ProcessPaymentsRequestMethods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static java.util.Optional.ofNullable;

@RestController
@RequestMapping("/wirecard")
public class PaymentController {

	@Autowired
	ProcessPaymentsRequestMethods paymentProcess;

	@PostMapping(path = "/payments")
	public ResponseEntity<String> createPayment(@RequestBody Payment payment) {
		String response = paymentProcess.process(payment);
		return new ResponseEntity<String>(response, (response.equals(StatusPayment.FAILURE.getStatus())) ?
				HttpStatus.UNPROCESSABLE_ENTITY : HttpStatus.CREATED);
	}

	@GetMapping(path = "/payments/{id_payment}/status")
	public ResponseEntity<Payment> paymentStatus(@PathVariable(value = "id_payment") String id) {
		Payment response = paymentProcess.findPayment(id);
		return ofNullable(response).map(
				execute -> new ResponseEntity<>(response, HttpStatus.OK))
		.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}
}