package com.wirecard.payment.adapter.repository;

import com.wirecard.payment.adapter.repository.data.PaymentData;
import com.wirecard.payment.adapter.repository.mongodb.PaymentRepositoryImp;
import com.wirecard.payment.domain.entity.Payment;
import com.wirecard.payment.usecases.port.PaymentRepositoryPort;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import static java.util.Optional.ofNullable;

@Slf4j
@Repository
public class PaymentCrudRepository implements PaymentRepositoryPort {

	private final PaymentRepositoryImp repository;

	private static final String PAYMENT_SAVED = "Payment saved with id: ";

	private static final String GET_PAYMENT_SUCCESS = "Retrieve payment success";

	@Autowired
	ModelMapper mapper;

	public PaymentCrudRepository(PaymentRepositoryImp repository){
		this.repository = repository;
	}
	
	@Transactional
	@Override
	public void save(Payment payment) {
		PaymentData paymentData = parseObjectToDataToSaveInDataBase(payment);
		repository.save(paymentData);
		log.info(PAYMENT_SAVED.concat(paymentData.getId()));
	}
	
	@Transactional
	@Override
	public Payment findById(String idPayment) {
		Optional<PaymentData> responseFromFindById = repository.findById(idPayment);
		Payment payment = prepareRetrieveFromFindById(responseFromFindById);
		log.info(GET_PAYMENT_SUCCESS);
		return payment;
	}

	private PaymentData parseObjectToDataToSaveInDataBase(Payment payment) {
		return  mapper.map(payment, PaymentData.class);
	}

	private Payment prepareRetrieveFromFindById(Optional<PaymentData> paymentData) {
		return ofNullable(paymentData).map(
				execute -> mapper.map(paymentData.get(), Payment.class))
				.orElse(null);
	}
}