package com.wirecard.payment.adapter.repository.data;

import com.wirecard.payment.domain.entity.Buyer;
import com.wirecard.payment.domain.entity.Card;
import com.wirecard.payment.domain.entity.PaymentType;
import com.wirecard.payment.domain.entity.StatusPayment;
import lombok.*;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Getter
@Setter
@Builder
@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Document(collation = "payment")
public class PaymentData implements Serializable {

    @Id
    private String id;
    private Double amount;
    private PaymentType paymentType;
    private String boletoNumber;
    private Card card;
    private StatusPayment statusPayment;
    private Buyer buyer;
}
