package com.wirecard.payment.adapter.repository.mongodb;

import com.wirecard.payment.adapter.repository.data.PaymentData;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PaymentRepositoryImp extends MongoRepository<PaymentData, String> {

}
