package com.wirecard.payment.adapter.creditcard;

import com.wirecard.payment.domain.entity.Payment;
import com.wirecard.payment.usecases.port.CreditCardClientPort;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class CreditCardClientPortImplement implements CreditCardClientPort {
	
	private static final Double value = 100.00;

	@Override
	public Boolean checkPaymentAllowed(Payment payment) {
		log.debug("checking payment is allowed.");
		return (payment.getAmount() >= value);
	}
}
