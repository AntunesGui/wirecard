package com.wirecard.payment.usecases;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.wirecard.payment.domain.entity.Payment;
import com.wirecard.payment.domain.entity.PaymentType;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class ValidatePaymentMethodsTest {
	
	@InjectMocks
	private ValidatePaymentMethod validadeInjects;

	@Mock
	private ValidatePaymentMethod validade;
	
	@Test
	public void is_card_payment_method_true() {
		
		Payment payment = new Payment();
		payment.setPaymentType(PaymentType.CARD);
		lenient().when(validade.isCardPaymentMethod(payment)).thenReturn(true);
		
		boolean response = validadeInjects.isCardPaymentMethod(payment);
		
		assertEquals(true, response);
	}
	
	@Test
	public void is_card_payment_method_false(){
		
		Payment payment = new Payment();
		payment.setPaymentType(PaymentType.BOLETO);
		lenient().when(validade.isCardPaymentMethod(payment)).thenReturn(false);
		
		boolean response = validadeInjects.isCardPaymentMethod(payment);
		
		assertEquals(false, response);
	}
	
	@Test
	public void is_boleto_payment_method_true(){
		
		Payment payment = new Payment();
		payment.setPaymentType(PaymentType.BOLETO);
		lenient().when(validade.isCardPaymentMethod(payment)).thenReturn(true);
		
		boolean response = validadeInjects.isBoletoPaymentMethod(payment);
		
		assertEquals(true, response);
	}
	
	@Test
	public void is_boleto_payment_method_false(){
		
		Payment payment = new Payment();
		payment.setPaymentType(PaymentType.CARD);
		lenient().when(validade.isCardPaymentMethod(payment)).thenReturn(false);
		
		boolean response = validadeInjects.isBoletoPaymentMethod(payment);
		
		assertEquals(false, response);
	}
}
