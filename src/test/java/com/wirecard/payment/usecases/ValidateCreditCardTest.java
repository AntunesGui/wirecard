package com.wirecard.payment.usecases;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import com.wirecard.payment.domain.entity.Card;
import com.wirecard.payment.domain.entity.Payment;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class ValidateCreditCardTest {
	
	@InjectMocks
	private ValidateCreditCard validateCreditCard;
	
	@Test
	public void is_card_valid_success() {
		Payment payment =  preparePaymenteCardNumberOk();
		Boolean response = validateCreditCard.isCardValid(payment);
		
		assertEquals(response, true);
	}
	
	@Test
	public void is_card_valid_failure() {
		Payment payment =  preparePaymenteCardNumberNotOk();
		Boolean response = validateCreditCard.isCardValid(payment);
		
		assertEquals(response, false);
	}
	
	private Payment preparePaymenteCardNumberOk() {
		Payment payment = new Payment();
		Card card = new Card();
		card.setNumber("4412-1222-6617-1285");
		card.setHolderName("Gui");
		payment.setCard(card);
		return payment;
	}
	
	private Payment preparePaymenteCardNumberNotOk() {
		Payment payment = new Payment();
		Card card = new Card();
		card.setNumber("4412-1222-6617-128");
		card.setHolderName("Gui");
		payment.setCard(card);
		return payment;
	}
}
