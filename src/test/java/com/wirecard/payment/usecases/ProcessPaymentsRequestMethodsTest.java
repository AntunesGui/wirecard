package com.wirecard.payment.usecases;

import com.wirecard.payment.adapter.repository.PaymentCrudRepository;
import com.wirecard.payment.domain.entity.Payment;
import com.wirecard.payment.domain.entity.PaymentType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class ProcessPaymentsRequestMethodsTest {
	
	@InjectMocks
	private ProcessPaymentsRequestMethods processPaymentsInjects;
	
	@Mock
	private ValidatePaymentMethod validate;

	@Mock
	private CheckCardPaymentTransaction cardTransaction;

	@Mock
	private PaymentCrudRepository repository;
	
	@Mock
	private ValidateCreditCard validateCreditCard;
	
	@Test
	public void process_boleto_payment_method() {
		Payment payment = preparePaymentByBoleto();
		//lenient().when(validate.isBoletoPaymentMethod(payment)).thenReturn(true);
		//lenient().doNothing().when(repository).save(payment);
		
		//String response = processPaymentsInjects.process(payment);
		
		//assertEquals(response, StatusPayment.BOLETONUMBER.getStatus());
	}
	
	@Test
	public void process_card_payment_method_allowed() {
		Payment payment = preparePaymentByCard();
		//lenient().when(validate.isCardPaymentMethod(payment)).thenReturn(true);
		//lenient().when(cardTransaction.checkPaymentTransactionIsAllowed(payment)).thenReturn(true);
		//lenient().when(validateCreditCard.isCardValid(payment)).thenReturn(true);
		//lenient().doNothing().when(repository).save(payment);
		
		//String response = processPaymentsInjects.process(payment);
		
		//assertEquals(response, StatusPayment.SUCCESS.getStatus());
	}
	
	@Test
	public void process_card_payment_method_not_allowed() {
		Payment payment = preparePaymentByCard();
		//lenient().when(validate.isCardPaymentMethod(payment)).thenReturn(true);
		//lenient().when(cardTransaction.checkPaymentTransactionIsAllowed(payment)).thenReturn(false);
		//lenient().doNothing().when(repository).save(payment);
		
		//String response = processPaymentsInjects.process(payment);
		
		//assertEquals(response, StatusPayment.FAILURE.getStatus());
	}
	
	private Payment preparePaymentByBoleto() {
		Payment payment = new Payment();
		payment.setPaymentType(PaymentType.BOLETO);
		return payment;
	}
	
	private Payment preparePaymentByCard() {
		Payment payment = new Payment();
		payment.setPaymentType(PaymentType.CARD);
		return payment;
	}
}
