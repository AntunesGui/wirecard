package com.wirecard.payment.adapter.webservice;

import com.wirecard.payment.domain.entity.Payment;
import com.wirecard.payment.domain.entity.PaymentType;
import com.wirecard.payment.domain.entity.StatusPayment;
import com.wirecard.payment.usecases.ProcessPaymentsRequestMethods;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class PaymentControllerTest {

	@InjectMocks
	private PaymentController paymentController;

	@Mock
	private ProcessPaymentsRequestMethods paymentProcess;

	@Test
	public void payment_by_card_transaction_allowed() {
		Payment payment = preparePaymentByCardObject();
		lenient().when(paymentProcess.process(payment)).thenReturn(StatusPayment.SUCCESS.getStatus());
		ResponseEntity<String> response = paymentController.createPayment(payment);
		
		assertEquals(new ResponseEntity<String>(StatusPayment.SUCCESS.getStatus(), HttpStatus.CREATED), response);
	}
	
	@Test
	public void payment_by_boleto_transaction_success() {
		Payment payment = preparePaymentByBoletoObject();
		lenient().when(paymentProcess.process(payment)).thenReturn(StatusPayment.SUCCESS.getStatus());
		ResponseEntity<String> response = paymentController.createPayment(payment);
		
		assertEquals(new ResponseEntity<String>(StatusPayment.SUCCESS.getStatus(), HttpStatus.CREATED), response);
	}

	@Test
	public void payment_failure() {
		Payment payment = preparePaymentByCardObject();
		lenient().when(paymentProcess.process(payment)).thenReturn(StatusPayment.FAILURE.getStatus());
		ResponseEntity<String> response = paymentController.createPayment(payment);
		
		assertEquals(new ResponseEntity<String>(StatusPayment.FAILURE.getStatus(), HttpStatus.UNPROCESSABLE_ENTITY), response);
	}
	
	private Payment preparePaymentByCardObject() {
		Payment payment = new Payment();
		payment.setPaymentType(PaymentType.CARD);
		return payment;
	}
	
	private Payment preparePaymentByBoletoObject() {
		Payment payment = new Payment();
		payment.setPaymentType(PaymentType.BOLETO);
		payment.setBoletoNumber(StatusPayment.BOLETONUMBER.getStatus());
		return payment;
	}
}
