package com.wirecard.payment.domain.entity;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

public class BuyerTest {
	
	@Test
	public void buyer_get_and_set_test() {
		Buyer buyer = new Buyer();
		Client client = new Client();
		buyer.setClient(client);
		buyer.setCpf(123456L);
		buyer.setEmail("gui@wirecard.com");
		buyer.setName("Gui");
		
		assertEquals(client, buyer.getClient());
		assertEquals(123456L, buyer.getCpf());
		assertEquals("gui@wirecard.com", buyer.getEmail());
		assertEquals("Gui", buyer.getName());
	}
}
