This README will help to perform the step by step for the configuration,
build and run the project on your machine.

A - (Install and open Eclipse or Spring Tools)

01 - On your machine open the browser and insert the 
link: https://spring.io/tools.
02 - Choose the download option that matches your operating system.
03 - With download completed, go to the download location and run the installer.
04 - Wait for the installation to complete.
05 - On the desktop look for the green icon called "Spring Tool"
and click on it.

B - (Clone the Gitlab Project)

01 - Create a folder where you want on your machine, to clone the project.
02 - On your machine, open git bash or the command line.
03 - To install "git bash", follow the link: https://git-scm.com/downloads
04 - If you choose use "git bash", make the install and wait to complete/
05 - Run the command in your "git bash" or Terminal: 
git clone https://gitlab.com/AntunesGui/wirecard".

C - (Set up the project in the IDE Spring Tool)

01 - With Spring Tool open, right-click inside the tab
"Project Explorer".
02 - Select -> "import" -> "import ...".
03 - With the "Import" window open, expand the option "Maven", select the
"Existing Maven Projects" option.
04 - With the "Import Maven Projects" window open, click "Browse ...".
05 - Browse to the folder where the git project clone was made.
06 - Back in the "Import Projects" window, inside the "Projects:" combo,
Select the combobox for the pom.xml file.
07 - Click on "Finish".

D - (Project Architecture)

01 - The "CLEAN ARCHITECTURE" model was chosen for the development of this
challenge.

02 - Modulo Domain: Is a set of related business rules that are critical to the 
function of the application. In an object oriented programming language the 
rules for an entity would be grouped together as methods in a class. 
Even if there were no application, these rules would still exist.

03 - Modulo Use cases: The use cases are the business rules for a 
specific application. They tell how to automate the system. 
This determines the behavior of the app. Package "port": Note, that the 
interface is also known as a port, as it makes the bridge between the business 
logic and the outside world.

04 - Modulo Adapters: The adapters, also called interface adapters, 
are the translators between the domain and the infrastructure. 
For example, they take input data from the GUI and repackage it in a form that 
is convenient for the use cases and entities. Then they take the output from 
the use cases and entities and repackage it in a form that is convenient for 
displaying in the GUI or saving in a database.

E - (Install Postman)

01 - To run the project, you need to install the Postman tool.
02 - To install the tool, access the
link: https://www.getpostman.com/downloads/

F - (Run the project)

*It is important to perform the steps below without turning off 
the application as persistence data are in memory* 

01 - With Spring Tool open, open the class called: "PaymentApplication".

02 - Click with the right mouse button. Select the option: "Run as",
"Spring Boot App".

03 - Wait for the end of the application start.

04 - With Postman open, import the file named: "Wirecard.postman_collection.json"
, to import click the "import" button, select where is the cloned postman file
next to the project.

05 - Inside the folder "Wirecard", we have four collections.

06 - "Payments by card with number ok" will execute and create a
card payment successfully will generate a payment id in the IDE console
and store the payment in a HashMap.

07 - "Payments by boleto", will execute and create a
bill payment successfully and will return the billet number and will generate a
payment id in the IDE console.

08 - "Return payments status", will query the payment status, to see
it is necessary to take the ID generated in the previous transactions in 
the IDE console and put in the URL.

09 - "Payments by card with number not ok", will perform a number validation
card, in this collection the number is already incorrect and will return in 
the IDE console The issuer name is whether the card is valid or not.



